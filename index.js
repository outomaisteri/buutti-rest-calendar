/*
Small REST-calendar API
-----------------------

Uses Node.js runtime

Install Node.js and run "node index" OR "npx nodemon index" in command-line

Server listens to http://localhost:1234/api/events/

Search events by name: ...events/xxx
Search events by query: ...events/?xxx&yyy&zzz
Accepted queries:
  year=xxxx
  month=xx
  day=xx
  time=xx:xx (shows ongoing events)

POSTing events tested with Postman (x-www-form-urlencoded)
Reuired fields:
  name = xxx
  startDateTime = dd.mm.YYYY HH:MM
  lengthInHours = x

DELETE events by eventID: .../events/x (tested with Postman)

Event information stored in ./sqlite/calendar.sqlite
Example events are generated on server start
  => calendar.sqlite can be deleted for fast database formatting

*/


// Set up Express & other middleware
const APP = require('express')();
const BODY_PARSER = require('body-parser');
const DB = require('./sqlite/database');

DB.initializeDatabase();
// Set up JSON BodyParser
APP.use(BODY_PARSER.urlencoded({extended: true}));
// Import API routes
APP.use('/api', require('./api'));
// Start up server
APP.listen(1234, function() {
  console.log('SUCCESS: Listening for http://localhost:1234/api/events/...');
});
