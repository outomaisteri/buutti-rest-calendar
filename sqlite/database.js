
const SQLITE = require('sqlite3').verbose();
//strftime('%d.%m.%Y %H:%M', startDateTime)

exports.initializeDatabase = function() {
  // Open calendar database, or create it if needed
  let database = new SQLITE.Database( './sqlite/calendar.sqlite', SQLITE.OPEN_READWRITE | SQLITE.OPEN_CREATE, (err) => {
      if (err) {
        console.error(err.message);
      }
      console.log('SUCCESS: Connected to calendar database (./sqlite/calendar.sqlite)');
  });
  // Run statements serially
  database.serialize(() => {
    // Create events table if needed
    database.run("CREATE TABLE IF NOT EXISTS events (eventID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, startDateTime DATE, lengthInHours INTEGER)", function(err) {
      if (err)  {
        console.error(err.message);
      }
    });
    // Insert test values into database
    database.run("INSERT OR IGNORE INTO events VALUES (1, 'Summer Fishing Trip',    '2018-06-10 12:00', '6')");
    database.run("INSERT OR IGNORE INTO events VALUES (2, 'Christmas Eve',          '2018-12-24 00:00', '23')");
    database.run("INSERT OR IGNORE INTO events VALUES (3, 'Office Christmas Party', '2018-12-14 16:00', '4')");
    database.run("INSERT OR IGNORE INTO events VALUES (4, 'USA day trip 2019',      '2019-03-05 03:00', '18')");
    console.log('Initialized calendar database');

    database.close((err) => {
        if (err) {
          console.error(err.message);
        }
        console.log('SUCCESS: Closed calendar database connection.');
    });
  });
};


// Get event from calendar based on query
exports.getEventsByQuery = function(query, callback) {
  let database = new SQLITE.Database('./sqlite/calendar.sqlite');
  var yearReq = query.year;
  var monthReq = query.month;
  var dayReq = query.day;
  var timeReq = query.time;
  var sqlBase = "";
  var sqlQuery = "";

  // Construct SQL query base
  if (Object.keys(query).length > 0) {
    sqlBase = 'SELECT * FROM events WHERE ';
  }
  else {
    sqlBase = 'SELECT * FROM events';
  }
  // Construct SQL query with parameters
  if (yearReq != undefined) {
    sqlQuery = 'strftime("%Y", startDateTime)=$year'
  }
  if (monthReq != undefined) {
    if (sqlQuery != '') {
      sqlQuery += ' AND ';
    }
    sqlQuery += 'strftime("%m", startDateTime)=$month';
  }
  if (dayReq != undefined) {
    if (sqlQuery != '') {
      sqlQuery += ' AND ';
    }
    sqlQuery += 'strftime("%d", startDateTime)=$day';
  }
  if (timeReq != undefined) {
    if (sqlQuery != '') {
      sqlQuery += ' AND ';
    }
    // datetime(strftime("%s", $startDateTime) + $lengthInHours * 3600, "unixepoch")
    sqlQuery += '($time BETWEEN strftime("%H:%M", startDateTime) AND strftime("%H:%M", datetime(strftime("%s", startDateTime) + lengthInHours * 3600, "unixepoch")))';
  }
  // Retreive events from calendar database
  database.all(sqlBase + sqlQuery,
    {
      $year: yearReq,
      $month: monthReq,
      $day: dayReq,
      $time: timeReq
    },
    (err, rows) => {
      console.log('Retreiving events by query...');
      if (rows.length > 0) {
        callback(rows);
      }
      else {
        callback("No results");
      }
    }
  );
  database.close();
};


// Get event from calendar based on name
exports.getEventsByName = function(name, callback) {
  let database = new SQLITE.Database('./sqlite/calendar.sqlite');
  // Get all events that contain search term
  var sql = 'SELECT * FROM events WHERE name LIKE "%' + name + '%"';
  database.all(sql, (err, rows) => {
      console.log('Retreiving events by name...');
      if (rows.length > 0) {
        callback(rows);
      }
      else {
        callback("No results");
      }
    }
  );
  database.close();
};

// Add new event
exports.addNewEvent = function(body, callback) {
  let database = new SQLITE.Database('./sqlite/calendar.sqlite');
  // Contert dates: dd.mm.YYYY HH:MM => YYYY.mm.dd HH:MM
  var convertedTime = body.startDateTime.substring(6, 10) + "-" + body.startDateTime.substring(3, 5) + "-" + body.startDateTime.substring(0, 2) + body.startDateTime.substring(10);
  database.run('INSERT INTO events (name, startDateTime, lengthInHours) VALUES ($name, $startDateTime, $lengthInHours)',
    {
      $name: body.name,
      $startDateTime: convertedTime,
      $lengthInHours: body.lengthInHours
    },
    (err) => {
      if (err) {
          callback("An error has occured:\n" + err.message);
      }
      else {
        callback("Successfully added a new event!");
      }
    }
  );
  database.close();
};


// Remove event by eventID
exports.removeEvent = function(eventID, callback) {
  let database = new SQLITE.Database('./sqlite/calendar.sqlite');
  database.run('DELETE FROM events WHERE eventID = $eventID', { $eventID: eventID },
    (err) => {
      if (err) {
          callback("An error has occured:\n" + err.message);
      }
      else {
        callback("Successfully removed an event!");
      }
    }
  );

  database.close();
};
