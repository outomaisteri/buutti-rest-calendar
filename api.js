// Set up Express router
const router = require('express').Router();
const DB = require('./sqlite/database');

// Get events from SQLite database with ?query
router.get('/events/', function(request, response) {
  DB.getEventsByQuery(request.query, function(rows) {
    console.log(rows);
    response.send(rows);
  });
});
// Get events from SQLite database by name
router.get('/events/:name', function(request, response) {
  DB.getEventsByName(request.params.name, function(rows) {
    console.log(rows);
    response.send(rows);
  });
});

// Add a new event to SQLite database
router.post('/events', function(request, response) {
  DB.addNewEvent(request.body, function(message) {
    console.log(message);
    response.send(message);
  });
});

// Update existing event in SQLite database
router.put('/events/:eventID', function(request, response) {
  response.send({type: 'PUT'});
});

// Delete event from SQLite database
router.delete('/events/:eventID', function(request, response) {
  DB.removeEvent(request.params.eventID, function(message) {
    console.log(message);
    response.send(message);
  });
});

// Export routes to index.js
module.exports = router;
